Page({

  onLoad: function (options) {
    this.check();
  },

  check: function() {
    wx.showLoading({title: '加载中'});
    var userInfo = wx.getStorageSync('userInfo');
    var code2seesion = wx.getStorageSync('code2seesion');
    if (!userInfo || !code2seesion) {
      wx.navigateBack();
      return false;
    }
    this.setData({
      userInfo: userInfo,
      code2seesion: code2seesion
    });
    wx.hideLoading();
  },

  decryptPhoneNumber: function(e) {
    var that = this;
    // 用户取消授权
    if (e.detail.errMsg == 'getPhoneNumber:fail user deny'){
      wx.showToast({title: '取消授权', icon: 'none'});
      return false;
    }
    wx.showLoading({title: '登录中'});
    let params = {
      iv: e.detail.iv,
      encryptedData: e.detail.encryptedData,
      code2seesion: that.data.code2seesion,
      user_info: JSON.stringify(that.data.userInfo)
    };

    wx.request({
      url: 'http://study.wxapplogin.localhost.com/api/account/wx_login2',
      method: 'post',
      data: params,
      success: function(res) {
        wx.hideLoading();
        // 异常
        if (res.data.code == 400) {
          wx.showToast({
            title: res.message,
            icon: 'none',
          });
          return false;
        }
        wx.setStorageSync('user_token', res.data.data.user_token);
        wx.navigateBack({
          delta: 2
        });
      }
    })
  },

})