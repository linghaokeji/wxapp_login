Page({

  data: {
    logined: false,
    user: {}
  },

  onShow: function() {
    this.getUser();
  },

  getUser: function() {
    let params = {};
    params['user_token'] = wx.getStorageSync('user_token');
    console.log(params);
    let that = this;
    wx.request({
      url: 'http://study.wxapplogin.localhost.com/api/account/getLoginUser',
      method: 'post',
      data: params,
      success (res) {
        if (res.data.data.id) {
          that.setData({
            logined: true,
            user: res.data.data
          })
        } else {
          that.setData({ logined: false });
        }
      }
    })
  },

  jumpPage: function(e) {
    let url = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: url,
    })
  },

})