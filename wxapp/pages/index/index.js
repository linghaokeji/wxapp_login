// index.js
// 获取应用实例
const app = getApp()

Page({
  
  jumpPage: function(e) {
    let url = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: url,
    })
  },

  switchTab: function(e) {
    let url = e.currentTarget.dataset.url;
    wx.switchTab({
      url: url,
    })
  }

})
