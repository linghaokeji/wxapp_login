Page({

  getUserProfile: function() {
    let that = this;
    wx.getUserProfile({
      desc: '用于完善用户信息',
      success: (userRes) => {
        if (userRes.userInfo) {

          wx.login({
            success (res) {
              if (res.code) {
                wx.request({
                  url: 'http://study.wxapplogin.localhost.com/api/account/wx_login1',
                  method: 'post',
                  data: {
                    code: res.code
                  },
                  success: function(res) {
                    // 异常
                    if (res.data.code == 400) {
                      wx.showToast({
                        title: res.message,
                        icon: 'none',
                      });
                      return false;
                    }

                    // 老账户
                    if (res.data.data.user_token != '') {
                      wx.setStorageSync('user_token', res.data.data.user_token);
                      wx.navigateBack({
                        delta: 1
                      });
                    }

                    // 新账户
                    if (res.data.data.code2seesion != '') {
                      wx.setStorageSync('userInfo', userRes.userInfo);
                      wx.setStorageSync('code2seesion', res.data.data.code2seesion);
                      wx.navigateTo({
                        url: "/pages/account/login_get_phone"
                      })
                    }
                  }
                })
              } else {
                console.log('登录失败！' + res.errMsg)
              }
            }
          })

        }
      }
    })
  },

})